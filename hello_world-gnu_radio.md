# Log in to the POWDER portal and environment setup

1.  Point your browser at the POWDER portal and log in:

	[https://www.powderwireless.net](https://www.powderwireless.net)

2.  If you didn't upload an ssh public key when you first created your
	POWDER account, now would be a good time to do that.
	
	(a) Generate an ssh key pair:
	
    ##### For Windows users
	
	A good option for a Windows ssh client is to make use of putty:
	
	[https://www.ssh.com/ssh/putty/](https://www.ssh.com/ssh/putty/) 
	
	To generate and manage ssh keys with putty:
	[https://www.ssh.com/ssh/putty/windows/puttygen](https://www.ssh.com/ssh/putty/windows/puttygen)
	
    ##### For Linux and Mac OS users
	
	To generate and manage ssh keys:
	
	[https://www.ssh.com/ssh/keygen/](https://www.ssh.com/ssh/keygen/)
	
	(b) Load the generated ssh public key into the POWDER portal:
	
	   Click on your username (top right), select `Manage SSH keys` and follow the prompts to load the ssh public key.

3.  Some of the hands-on activities on POWDER require X windows. We provide a VNC-based solution for this (see below). However, it might be useful to have an X-server running on your laptop.

	##### For Windows users
	
	Various options are available, for example:
	
	[https://sourceforge.net/projects/vcxsrv/](https://sourceforge.net/projects/vcxsrv/)
	
	##### For Mac OS users
	
	[https://www.xquartz.org](https://www.xquartz.org)
	
	
	
# ``Hello World'' - Using GNU Radio in simulation mode 

1.  Start an experiment (use the “Experiments” menu “Start Experiment” entry if necessary), and change to the GNURADIO-SIM profile. Select the “Emulab” cluster, and a duration of 2 hours. (Or, click [here](https://www.powderwireless.net/p/PowderProfiles/GNURADIO-SIM) to go directly to the profile.)

1.  Once the experiment is ready (several minutes), choose the only node in the experiment and open a browser shell. Within the shell, execute:

	```
	/share/powder/runvnc.sh
	```
	Use a temporary password for the requested password.

1.  Open the link given in the shell, of the form shown below (i.e., use the link that looks like  this *in your experiment*, not the one below):

    https://node.foo.mww2019.emulab.net:8787/vnc_auto.html

    You will probably have to persuade your browser to accept the node's self-signed certificate. And you will have to re-enter the password you provided in the previous step.

1.  Within a terminal in the resulting VNC desktop, execute:
    ```
    gnuradio-companion /share/powder/tutorial/psk.grc
    ```
    You will be able to manipulate and run the GNU Radio Companion flow graph on the remote system.

1.  Assuming you have uploaded ssh keys, have an ssh client program and have X11 working on your laptop: 
	
	Repeat the previous step, but rather than using the VNC desktop, use an ssh client on your laptop to directly ssh into the node in your experiment. Then execute the gnuradio-companion command.

1.  When you are finished, return to the experiment page and press the `Terminate` button.
	

	