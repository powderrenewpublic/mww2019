# FCC spectrum license lookup and measurement

#### FCC spectrum database search

1. Find the frequency ranges used by LTE FDD in band 30. E.g., from here:
   
   [https://en.wikipedia.org/wiki/LTE_frequency_bands](https://en.wikipedia.org/wiki/LTE_frequency_bands)

<!--
	uplink: 2305 – 2315	downlink: 2350 – 2360
-->

2. Point your browser at the FCC spectrum database search page:
	
	[https://wireless2.fcc.gov/UlsApp/UlsSearch/searchLicense.jsp](https://wireless2.fcc.gov/UlsApp/UlsSearch/searchLicense.jsp)
	
3. Select the `Geographic` search option.

4. Enter the following search criteria:

	(a) State: Utah

	(b) County: UT - Salt Lake

	(c) Enter a frequency range that covers both the uplink and downlink for band 30.

<!--
	2300 - 2360
	
	Results:
	
	KNLB260 - New Cingular Wireless Services, Inc.
	WS - Wireless Communications Service
	A  
	002305.00000000-002310.00000000 
	002350.00000000-002355.00000000 
	
	KNLB261 - New Cingular Wireless Services, Inc.
	B 
	WS - Wireless Communications Service
	002310.00000000-002315.00000000 
	002355.00000000-002360.00000000 
	
	KNLB300 - New Cingular Wireless PCS, LLC
	C  
	WS - Wireless Communications Service
	002315.00000000-002320.00000000 
	
	KNLB301 - New Cingular Wireless PCS, LLC
	D
	002345.00000000-002350.00000000 
	
-->

5. Explore the results:
	
	(a) What radio service is associated with the licenses?
	
	(b) What channels and frequency ranges are covered by each license?
	
	(c) What frequency ranges in both upstream and downstream are collectively covered by these licenses?

		
<!--
	
	WS - Wireless Communications Service
	https://www.fcc.gov/wireless/bureau-divisions/mobility-division/wireless-communications-service-wcs
	
	upstream: 2305 - 2320
	downstream: 2345 - 2360
	
-->

#### Spectrum measurement using uhd_fft

For this session we will be using cellsdr rooftop and the fixed-endpoint nuc2 nodes:

| Group Number  | Experiment Name |
| --- | --- |
| 1 |  cell-bes |
| 2 |  cell-browning |
| 3 |  cell-smt |
| 4 | madsen |
| 5 | bookstore |
| 6 | ebc |
| 7 |  humanities |
| 8 |  law73 |
| 9 |  moran |



1. *Representative from each group* log into the compute node associated with your SDR experiment. Run `uhd_fft` to try to determine whether these licenses are in use.

	(a) Select a center frequency "close" to the range you want to observe.

	(b) Select a sampling rate appropriate for the SDR in your experiment. (For fixed-endpoints use a sample rate of 50 MS/s, i.e., `-s 50M`. For base stations use a sample rate of 100 MS/s, i.e., `-s 100M`.)

	(c) Execute `uhd_fft`:
	```
	uhd_fft -s SAMPLE_RATE -f CENTER_FREQUENCY
	```
	Note: You might have to run uhd_fft a number of times with different parameters to properly cover both the upstream and downstream frequency ranges. (Or, adjust it from within the GUI.)
	
<!--
	X310: 
	uhd_fft -s 100M -f 2330M
	
	B210:
	uhd_fft -s 50M -f 2310M
	uhd_fft -s 50M -f 2340M
-->	
	
	
2. Explore the results:

	(a) Is there activity on the downstream frequency range?

	(b) What about the upstream?

	(c) Any other noteworthy activity?
	
<!--
	Clear activity on part of downstream: 2350 - 2360.
	Upstream less clear, especially on fixed-endpoints.
	Nice peaks at 2325MHz (5MHz) and 2338MHz (5MHz) 
	These do not seem to be covered by any of the licenses
	we find in the search...
-->	 

#### More spectrum license exploration

1. Perform similar activities (i.e., FCC database search followed by `uhd_fft` measurements) for a `Market Based` license search with: 
		
	* Market Type: BEA
	* Market: BEA152 - Salt Lake City-Ogden, UT-ID
	* Radio service: AD - AWS-4 (2000-2020 MHz and 2180-2200 MHz)
		
<!---
	Results:
	
	Get two:
	T060430152 - Gamma Acquisition L.L.C.
	002000.00000000-002010.00000000 
	002180.00000000-002190.00000000 

	T070272152 - DBSD Corporation
	002010.00000000-002020.00000000 
	002190.00000000-002200.00000000 

	Doesn't look like these are in use...
	
	However, should see a strong signal from 2160 to 2180...
--->		

2. Do a `Geographic` search with:
	
	* State: Utah
	* County: UT - Salt Lake
	* Frequency range: 2160 - 2180
		
<!--
	Result:
	
	3 active ones with:
AT - AWS-3 (1695-1710 MHz, 1755-1780 MHz, and 2155-2180 MHz)

WQVN856 - AT&T Mobility Spectrum LLC
001760.00000000-001765.00000000 
002160.00000000-002165.00000000 

WQVN857 - AT&T Mobility Spectrum LLC
001765.00000000-001770.00000000 
002165.00000000-002170.00000000 

WQVP220 - Cellco Partnership
001770.00000000-001780.00000000 
002170.00000000-002180.00000000 
-->

3. Possible explanation for difference between AD (AWS-4) and AT (AWS-3) observations:
	
[https://www.fcc.gov/auction/97](https://www.fcc.gov/auction/97)

[https://www.fcc.gov/document/aws-2000-20202180-2200-mhz-aws-4-order-adopted](https://www.fcc.gov/document/aws-2000-20202180-2200-mhz-aws-4-order-adopted)

[https://www.forbes.com/sites/fredcampbell/2018/07/20/dish-network-terrestrial-spectrum-licenses-at-risk/](https://www.forbes.com/sites/fredcampbell/2018/07/20/dish-network-terrestrial-spectrum-licenses-at-risk/)	

<!--
	AWS-4: FCC allowed Dish to convert this from one license type to another.
	But apparently they have not been very successful in using it.
-->