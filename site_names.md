Note that some site location names have synonyms that you should understand to request resources.

# Base Stations:
- bes: a.k.a. Behavioral
- browning: a.k.a Geology
- dentistry: 
- fm: a.k.a Friendship Manor
- honors: 
- meb: a.k.a. Merrill
- smt: a.k.a. Medical Tower
- ustar: 

# Fixed Endpoints:
- web: a.k.a. Warnock
- ebc: a.k.a. Eccles
- bookstore:
- law73:
- humanities:
- madsen:
- moran: 
- sage: a.k.a. Sage Point