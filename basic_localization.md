# Localization of a Transmitter Using RSS

### Problem Setup

If we want to locate a transmitter (source localization), we can use Powder to
make RSS measurements from whatever nodes we possibly can.  

We assume whatever method we use will need to know the coordinates of the receivers we're using.  We'll use lat/long (GPS) coordinates to make it easier to integrate with a map of the Powder area.  Some site coordinates are listed; but note that these are approximate, as they've been taken from Google Maps, perhaps with about 20-40 m error.

| Group | Site Name  | (Lat, Long) in degrees |
| --- | --- | --- |
| 1 | meb | (40.768796, -111.845994) |
| 2 | browning | (40.766326, -111.847727) |
| 3 | ustar | (40.768810, -111.841739) |
| 4 | bes | (40.761352, -111.846297) |
| 5 | fm | (40.758060, -111.853314) |
| 6 | honors | (40.763975, -111.836882) |
| 7 | smt | (40.767959, -111.831685) |
| 8 | dentistry | (40.758183, -111.831168) |
| 9 | law73 | (40.761714, -111.851914) |
| 10 | bookstore | (40.764059, -111.847511) |
| 11 |humanities | (40.763894, -111.844020) |
| 12 | web | (40.767902, -111.845535) |
| 13 | ebc | (40.767611, -111.838103) |
| 14 | sage | (40.762914, -111.830655) |
| 15 | madsen | (40.757968, -111.836334) |
| 16 | moran | (40.769830, -111.837768) |

We will make received power measurements at a set of these devices, which I also refer to as "sensors".  Our goal is to estimate where the transmission may be coming from, and what its transmit power might be.

### Turn on a Transmitter 

Two options for using these instructions:

1.  *Real-world Case*: Identify the location of an existing source.  That is, a source is transmitting within the band you're able to measure.  The goal is to estimate where that transmission is coming from.  You don't need to "turn on a transmitter", just identify the center frequency / frequency band of interest.

2.  *Test Case*: We will ask one group (selected randomly?) to run a CW transmitter on their device. That group should SSH in to that device as described in the [path loss measurement tutorial](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/blob/master/Path%20Loss%20Measurement.md) and use 
```python
uhd_siggen_gui --const --freq=3515M
```
to transmit a continuous-wave (CW signal).

### Make Measurements

1. The rest of the groups should log into their device and make measurements of the received power of the signal.  Again, copying the command from the [path loss measurement tutorial](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/blob/master/Path%20Loss%20Measurement.md):
```
uhd_fft -f 3515M -s 2M --lo-offset=1.2M
```

2. For the purposes of this tutorial, we have a [Google Sheet for you to put the value you measure](https://docs.google.com/spreadsheets/d/1pQtrW50ELMNfazVQOOWVQ7ypr30KVse4NtU_uLViz0M/edit?usp=sharing), so that we can collaboratively measure the RSS values.  Put your RSS value in column B next to the device name. 

3.  Download the Google Sheet as a .csv file.  Rename it to `test_basic.csv`.

In general, you can directly generate the measurement file.  Each measurement is one row of a file.  Type the following in a new row of this file:
```
<device_name>, <RSS>
```
where `device_name` is the name of the node, e.g., `web`, and `<RSS>` is the received power as a floating point number, e.g., -85.6.  The file will have as many rows as the measurements made.  


### Run the localization algorithm

4. Change the current directory on your local terminal window using `cd` to the directory with `test_basic.csv` in it.  Then put the data file on the remote computer by sftp:
```
sftp npatwari@pc838.emulab.net
```
replacing `npatwari` and `pc838` with your username and the pc address.  Then, once connected, do:
```
put test_basic.csv
```
exit out of sftp with a `exit` command.

6. In the window where you're connected via SSH to the remote computer, we need to upgrade Python3 to include two libraries that (for some reason) are not included by default (skip this if you've done it on this computer already):
```
sudo apt update
sudo apt install python3-pip
pip3 install matplotlib
pip3 install pandas
```
Next, make a (writeable) local copy of the script we need to run:
```
cp /share/mww2019/python/ml_source_imaging.py .
```

7. Finally, run the python script with:
```
python3 ml_source_imaging.py test_basic.csv
```
Three plot windows should pop up.  One shows the likelihood value at each pixel.  Another shows the estimated transmit power.  Finally, a third shows a map of the base stations, along with the estimated (MLE) coordinate, and the actual coordinate (if it was set to display the actual in the code).
