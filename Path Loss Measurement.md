*  For this session we will make use of cbrs rooftop nodes
nodes:

| Group Number   | Profile Name/Experiment Name | Center Frequency |
| --- | --- | --- |
| 1 | cbrs-dentistry | 3500e6 |
| 2 | cbrs-bes | 3500.1e6 |
| 3 |  cbrs-browning | 3500.2e6 |
| 4 | cbrs-fm | 3500.3e6 |
| 5 |  cbrs-honors | 3500.4e6 |
| 6 |  cbrs-meb | 3500.5e6 |
| 7 |  cbrs-smt | 3500.6e6 |
| 8 |  cbrs-ustar | 3500.7e6 |

1. A spectrum analyzer tool which uses a connected UHD device to display the spectrum at a given frequency. 
```
uhd_fft -f 3500.35e6 -s 2M --lo-offset=1.2M
```
2. Generate a constant carrier signal from a transmitter node
The first group starts transmitting first.  All other groups measure the RX power.  
As soon as Groups #2-8 are done recording RX power for the signal from Group #1, 
Group #1 should stop their receiver and Group #2 should start their transmitter.
A transmitter is set up using:
```python
uhd_siggen_gui --const --freq <x>
```
where \<x\> is replaced with the group's frequency in the rightmost column of the table.

3. Note the received power level from the Power Spectral Density plot from all 
the receiver nodes and enter into the 
[shared Google Sheet called SurveyTest](https://docs.google.com/spreadsheets/d/1ss9QkCH5-wWP0XMRpUH858arZ5n5BmUtnJrh_7Nl83A/edit?usp=sharing).  
If there are any measurements which are missing, 
please add the non-numeric string "x" so that this link is not considered to be measured.

4. When we all are done entering data into the sheet, download the sheet as a 
CSV file.  Name it (rename it) a filename of `SurveyTest.csv`.  

5. Change the current directory on your local terminal window using `cd` to the 
directory with `SurveyTest.csv` in it.  Then put the data file on the remote 
computer by sftp:
```
sftp npatwari@pc838.emulab.net
```
replacing `npatwari` and `pc838` with your username and the pc address.  Then, once connected, do:
```
put SurveyTest.csv
```
exit out of sftp with a `exit` command.

6. In the window where you're connected via SSH to the remote computer, we need to upgrade Python3 to include two libraries that (for some reason) are not included by default:
```
sudo apt update
sudo apt install python3-pip
pip3 install matplotlib
pip3 install pandas
```
Next, make a (writeable) local copy of the script we need to run:
```
cp /share/mww2019/python/plot_rss_vs_distance.py .
```
Edit this file to change the `filename` to `SurveyTest.csv`.  You can use your favorite editor, but of course we all know the only *real* editor is vi.

7. Finally, run the python script with:
```
python3 plot_rss_vs_distance.py
```
The plot should pop up, and it should echo to the screen the path loss exponent `n_p` and `std of error`.  
