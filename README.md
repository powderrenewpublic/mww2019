# Important

The content described/contained in this repository was developed for the [POWDER-RENEW Mobile and Wireless Week](https://www.powderwireless.net/mww2019).

It is left here as a reference for the community.

**Note however:**
 
 * The POWDER project used in the hands-on exercises (i.e., mww2019), was used only during the event and can no longer be used to access POWDER resources. (I.e., you can no longer join the project and even MWW attendees will no longer be able to use this POWDER project.)
 * In order to use POWDER (e.g., to try out the hands-on exercises), you will have to [request your own POWDER project](https://powderwireless.net/signup.php), or join a project created by someone in your organization. (E.g., your adviser.)
 * Other than that most hands-on instructions will work "as is". However, over-the-air operation will not be generally available till October 2019.
 

# POWDER-RENEW Mobile and Wireless Week 2019 (MWW 2019)

## Schedule

### Monday

* 8:30am - 10:00am *POWDER Overview and experimental workflow*

    * P1: Presentation: [POWDER Overview](https://docs.google.com/presentation/d/1yT69e3hl6jjSNmHAqwggwFliVVzLdmJVdosvfM4Q_60/edit?usp=sharing) (Kobus)
    * H1: Hands-on: **"Hello World" profile with GNU Radio** ([Instructions](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/blob/master/hello_world-gnu_radio.md) and  [Overview](https://docs.google.com/presentation/d/1n1FjHq8VQQYckLC-oLojWGLcGWlpE2Htj5n6NfmRJZE/edit?usp=sharing)) (Gary)
    * H2: Hands-on: **OAI 5G NR - simulated configuration** ([Instructions](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/blob/master/oai_5g_nr.md) and [Overview](https://docs.google.com/presentation/d/1n1FjHq8VQQYckLC-oLojWGLcGWlpE2Htj5n6NfmRJZE/edit?usp=sharing)) (Kirk)
<br/><br/>

* 10:00am - 10:30am Break (snacks and drinks) 

* 10:30am - noon *End-to-end Mobile Networking*

  * D1: Demo: **OAI 5G NR - controlled environment & over-the-air configuration** ([Overview](https://docs.google.com/presentation/d/1n1FjHq8VQQYckLC-oLojWGLcGWlpE2Htj5n6NfmRJZE/edit?usp=sharing)) (JC)
  * P2: Presentation: [4G/5G Mobile Network Architectures](https://docs.google.com/presentation/d/1Oz5oY6NLdQgb0VjVMBgUiNLbQypFCX7mWFvxWXRSyfo/edit?usp=sharing) (Kirk and Kobus)
  * H3: Hands-on: **End-to-end 4G mobile network - simulated RAN configuration** ([Instructions](/4G-LTE.md) and [Overview](https://docs.google.com/presentation/d/1n1FjHq8VQQYckLC-oLojWGLcGWlpE2Htj5n6NfmRJZE/edit?usp=sharing)) (Kirk)
<br/><br/>

* noon - 1:30pm Lunch (on your own)

* 1:30pm - 3:00pm *RAN Softwarization*

  * D2: Demo and presentation: **End-to-end 4G mobile network - over-the-air configuration** ([Overview](https://docs.google.com/presentation/d/1n1FjHq8VQQYckLC-oLojWGLcGWlpE2Htj5n6NfmRJZE/edit?usp=sharing)) (Alex)
  * P3: Presentation: [RAN Softwarization](https://docs.google.com/presentation/d/1xNkrqklKeFivfPexj1-TqGtMQQs2-Jj7yPtHpWdwUFQ/edit?usp=sharing) (Kobus)
  * H4: Hands-on: **RAN programmability with OAI and XRAN** ([Instructions](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/blob/master/ran_virtualization.md) and [Overview](https://docs.google.com/presentation/d/1n1FjHq8VQQYckLC-oLojWGLcGWlpE2Htj5n6NfmRJZE/edit?usp=sharing))(Kobus)
<br/><br/>

* 3:00pm - 3:30pm Break (snacks and drinks)

* 3:30pm - 5:00pm Mini-project presentation/discussion
    * [Mini project ideas](https://docs.google.com/document/d/1wgJWGgmcNlFVs3fBFLW9r9dHK0CxM6SMfdP1U8RD_7s/edit?usp=sharing)
    * [Spreadsheet for mini project interest](https://docs.google.com/spreadsheets/d/1uDGdvaK2q_pSz66wUquR6KMXR_IwcDlG0LQumnCvn6U/edit?usp=sharing)

<!---
* [Monday feedback](https://forms.gle/quq5Dw9XMTATvtrr8)

* 5:00pm - Field trip: WEB fixed-endpoint (Jon), MEB rooftop base station (Kirk/Alex)
--->

### Tuesday

* 8:30am - 10:00am *Over-the-air operation*
  * H5: Hands-on: **Over-the-air operation on POWDER** ([Instructions](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/blob/master/ota.md) and [Overview](https://docs.google.com/presentation/d/1n1FjHq8VQQYckLC-oLojWGLcGWlpE2Htj5n6NfmRJZE/edit?usp=sharing)) (Kobus)
  * P4: [Presentation: RF Measurements/Propagation/Channels](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/blob/master/MWW2019_Presentation_Neal.pdf) (Neal)
  * H6: Hands-on: [FCC license lookup and spectrum measurements](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/blob/master/fcc_license_lookup_measurement.md) (Kobus)
<br/><br/>
  
* 10:00am - 10:30am Break (snacks and drinks)

* 10:30am - noon *RF exploration with UHD and GNU Radio*
  * H7: [Hands-on: Path loss measurements](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/blob/master/Path%20Loss%20Measurement.md) (Ayaz)
  * P5: [Presentation: OFDM Basics](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/blob/master/MWW2019_Presentation_Neal.pdf) (Ayaz and Neal)
  * H8: [Hands-on: QPSK over OFDM](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/blob/master/QPSK%20over%20OFDM.md) (Ayaz)
<br/><br/>

* noon - 1:30pm Lunch (on your own)

* 1:30pm - 3:00pm *RF exploration with UHD and GNU Radio*
  * P6: [Presentation: Basic localization](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/blob/master/MWW2019_Presentation_Neal.pdf) (Neal and Shamik)
  * H9: Hands-on: [Basic localization](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/blob/master/basic_localization.md) (Neal)
<br/><br/>

* 3:00pm - 3:30pm Break (snacks and drinks)

* 3:30pm - 5:00pm Work on mini-projects

<!---
* [Tuesday feedback](https://forms.gle/Pfu94tzx7cUBRLLQA)
--->

### Wednesday

* 8:30am - 10:00am *Communications Basics - 1*

* [Presentation: Basic Wireless](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/blob/master/MATLAB_partials_POWDER/mww_slides.pdf) (Nico)
* [Hands-on: MWW_2019_LabHandout](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/blob/master/MATLAB_partials_POWDER/MWW_2019_Lab_handout.pdf) (Nico, Oscar)
<br/><br/>

* 10:00am - 10:30am Break (snacks and drinks)

* 10:30am - noon *Communications Basics - 2*

* [Presentation: Basic Wireless](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/blob/master/MATLAB_partials_POWDER/mww_slides.pdf) (Nico)
* [Hands-on: MWW_2019_LabHandout](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/blob/master/MATLAB_partials_POWDER/MWW_2019_Lab_handout.pdf) (Nico, Oscar)
<br/><br/>
* noon - 1:30pm Lunch (on your own)

* 1:30pm - 3:00pm *Antenna Design - 1* 
  * [Presentation: Introduction and theory](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/blob/master/antenna%20design/antenna_design.pptx) (David Schurig)
<br/><br/>

* 3:00pm - 3:30pm Break (snacks and drinks)

* 3:30pm - 5:00pm *Antenna Design - 2* 
  * [Hands-on: Using the CST simulation environment](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/raw/master/antenna%20design/antennas_hands_on.pdf?inline=false)
  (*Note: This session will take place in the Engman Lab - WEB L210*)
<br/><br/>
 
<!--- 
* [Wednesday feedback](https://forms.gle/yjcrqGK3vGzzThst5)
--->

### Thursday

* 8:30am - 10:00am *Communications Research - 1*

* [Presentation: Basic Wireless](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/blob/master/MATLAB_partials_POWDER/mww_slides.pdf) (Nico)
* [Hands-on: MWW_2019_LabHandout](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/blob/master/MATLAB_partials_POWDER/MWW_2019_Lab_handout.pdf) (Nico, Oscar, Rahman)
<br/><br/>

* 10:00am - 10:30am Break (snacks and drinks)

* 10:30am - noon *Communications Research - 2*

* [Presentation: Basic Wireless](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/blob/master/MATLAB_partials_POWDER/mww_slides.pdf) (Nico)
* [Hands-on: MWW_2019_LabHandout](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/blob/master/MATLAB_partials_POWDER/MWW_2019_Lab_handout.pdf) (Nico, Oscar, Rahman)
<br/><br/>

* noon - 1:30pm Lunch (on your own)

* 1:30pm - 3:00pm *Communications Research - 3*

* [Presentation: Basic Wireless](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/blob/master/MATLAB_partials_POWDER/mww_slides.pdf) (Nico)
* [Hands-on: MWW_2019_LabHandout](https://gitlab.flux.utah.edu/powderrenewpublic/mww2019/blob/master/MATLAB_partials_POWDER/MWW_2019_Lab_handout.pdf) (Nico, Oscar, Rahman)
<br/><br/>

* 3:00pm - 3:30pm Break (snacks and drinks)

* 3:30pm - 5:00pm Work on mini-projects

* 4:30pm - Tour of POWDER-RENEW facilities (groups of 15)

<!---
* [Thursday feedback](https://forms.gle/MWQfUK3S4jzpjb7t8)
--->

### Friday

* 8:30am - 10:00am Work on mini-projects

* 10:00am - 10:30am Break (snacks and drinks)

* 10:30am - noon Work on mini-projects

* noon - 1:30pm Lunch (on your own)

* 1:30pm - 3:00pm Work on mini-projects

* 3:00pm - 3:30pm Break (snacks and drinks)

* 2:45pm - 5:00pm Mini-project presentations/demo videos
  * [Overview](https://docs.google.com/presentation/d/1zh01z9KAr4IgXTmCcHhcJb-X05zcF4kyCS1u4fChnFQ/edit?usp=sharing)
  * [Joint project presentation](https://docs.google.com/presentation/d/15OCaRxBIWcpromqPDpv2aEdtortUbNYcoJlNWT96Qm4/edit?usp=sharing)

<!--- 
 * [Mini-project evaluation form](https://docs.google.com/forms/d/e/1FAIpQLSeiREecxcn5860nYNM4ODqstzzz5Ii8Zm3jkeRBzOxj8DfDiQ/viewform?usp=sf_link)

  * [Final overall feedback](https://forms.gle/EtFgoyvN9o7S1VMr8)
--->