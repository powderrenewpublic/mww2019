# Open Air Interface (OAI) 5G NR

#### Create a two-node POWDER profile with additional storage

1. Find the `single-pc` profile. (Click on `Docs`, select `Example Profiles`, scroll down and select the `single-pc` profile. Alternatively: Click on `Experiments`, select `Start Experiment`, click on `Change Profile` and enter `single-pc` in the search box.)
	
2. Rather than instantiating the profile, make your own copy by selecting the `Copy Profile` option. Include your username in the profile name (to prevent collisions).  
	
3. Modify (using `Edit Code`) your copy of the profile as follows:
	
	(a) We will have two nodes in our experiment, so change the the name of the existing node to reflect that:

	```
	node1 = request.RawPC("node1")
	```
		
	(b) Explicitly request the node image to be UBUNTU 18:
	
	```
	node1.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
	```
		
	(c) Request a specific hardware type:
	
	```
	node1.hardware_type = "d430"
	```

	(d) Request a network interface with a specific IP address/netmask: 
	
	```
	iface1 = node1.addInterface("if1")
	iface1.addAddress(rspec.IPv4Address("10.10.1.1", "255.255.255.0"))
	```
		
	(e) Request a 10GB block store for the node:
	
	```
	bs1 = node1.Blockstore("bs1","/mytempdata")
	bs1.size = "10GB"
	```
	
	(f) Request another node with similar configuration:
	
	```
	node2 = request.RawPC("node2")
	node2.disk_image = "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU18-64-STD"
	node2.hardware_type = "d430"
	iface2 = node2.addInterface("if1")
	iface2.addAddress(rspec.IPv4Address("10.10.1.2", "255.255.255.0"))
	bs2 = node2.Blockstore("bs2","/mytempdata")
	bs2.size = "10GB"
	```
		
	(g) Request a wired link between the two nodes:
	
	```
	wiredlink = request.Link("link")
	wiredlink.addInterface(iface1)
	wiredlink.addInterface(iface2)	
	```
		
4. Click `Accept` and then `Create` to save your new profile. (Or if you did `Create` previously, you would do `Accept` and then `Save`.) 

5. `Instantiate` your new profile.  On the `Schedule` page, stay with the default options and select `Finish`.

#### Download and compile the Open Air Interface (OAI) 5G repository

Roughly follow instructions from these OAI wiki pages to download and compile the code:

[Get source](https://gitlab.eurecom.fr/oai/openairinterface5g/blob/master/doc/GET_SOURCES.md)

[Build instructions](https://gitlab.eurecom.fr/oai/openairinterface5g/wikis/5g-nr-development-and-releases)

*Note*: For the exercise below we will use a slightly modified version of the OAI code. To use the unmodified OAI code replace the appropriate commands below as follows:

```
git clone https://gitlab.eurecom.fr/oai/openairinterface5g.git
git checkout develop-nr
```

1. On *both* nodes, execute the following commands to compile the OAI 5G NR branch for simulated operation:

	```
	sudo bash
	cd /mytempdata/
	git clone https://gitlab.flux.utah.edu/oai/openairinterface5g.git
	cd openairinterface5g
	git checkout develop-nr-working
	cd cmake_targets
	./build_oai -I --gNB --nrUE -w SIMU
	cd ran_build/build
	make rfsimulator
	```
	
2. Run the gNB on `node1`:

	```
	sudo bash
	cd /mytempdata/openairinterface5g/cmake_targets/ran_build/build/
	RFSIMULATOR=enb ./nr-softmodem -O ../../../targets/PROJECTS/GENERIC-LTE-EPC/CONF/gnb.band78.tm1.106PRB.usrpn300.conf --parallel-config PARALLEL_SINGLE_THREAD
	```
	
3. Run the UE on `node2`:

	```
	sudo bash
	cd /mytempdata/openairinterface5g/cmake_targets/ran_build/build/
	RFSIMULATOR=10.10.1.1 ./nr-uesoftmodem --numerology 1 -r 106 -C 3510000000 -E -d
	```

