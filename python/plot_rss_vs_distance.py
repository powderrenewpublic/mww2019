#! /usr/bin/env python

#
# LICENSE:
# Copyright (C) 2019  Neal Patwari
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#    
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#    
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
# 
# Author: Neal Patwari, neal.patwari@gmail.com
#
# Version History:
#
# Version 1.0:  Initial Release.  27 Oct 2014.
# Version 2.0:  For Powder MWW 2019.  5 Sep
#

import pandas
import numpy as np
import math
import matplotlib.pyplot as plt
import matplotlib

matplotlib.rc('xtick', labelsize=16) 
matplotlib.rc('ytick', labelsize=16) 


# INPUTS: coord1, coord2
#    These are (latitude, longitude) coordinates in degrees
#    As given by google maps, for example
# OUTPUT: distance in meters
# Copied from:
#    https://stackoverflow.com/questions/19412462/getting-distance-between-two-points-based-on-latitude-longitude
#
def calcDistLatLong(coord1, coord2):

    R = 6373000.0 # approximate radius of earth in meters

    lat1 = math.radians(coord1[0])
    lon1 = math.radians(coord1[1])
    lat2 = math.radians(coord2[0])
    lon2 = math.radians(coord2[1])

    dlon = lon2 - lon1
    dlat = lat2 - lat1

    a    = math.sin(dlat / 2)**2 + math.cos(lat1) * math.cos(lat2) * math.sin(dlon / 2)**2
    dist = R * 2 * math.atan2(math.sqrt(a), math.sqrt(1 - a))

    return dist


# INPUTS: 
#     rss_array: numpy array of signal strength measurements, in dB (reference arbitrary)
#     dist_array: distance / path length corresponding to each RSS (in same distance units as d0)
#     d0: reference distance (in same units as dist_array)
#
# OUTPUTS:
#     n_p and RSS0: follow model that
#       rss_array[i] = RSS0 - 10.0 * n_p * log10(dist_array[i]/d0) + e
#    
def calcPLexponent(rss_array, dist_array, d0=1, printOption=True):

	# Model says RSS is linear in dB distance
    dist_dB = -10.0*np.log10(dist_array/d0)
    #print(dist_dB)
    #print(rss_array)
    coeffs  = np.polyfit(dist_dB, rss_array, 1)
    n_p     = coeffs[0]
    RSS0    = coeffs[1]

    # Compute the standard deviation of error
    rss_est = n_p*dist_dB + RSS0
    residuals = rss_array - rss_est
    e_std   = np.sqrt(np.mean(np.abs(residuals)**2))

    #Plot the results
    if printOption:
        plt.ion()
        plt.semilogx(dist_array, rss_array, 'ro', dist_array, rss_est, 'b-')
        plt.grid('on')
        #plt.xlim(100, 2000)
        plt.xlabel('Path Length (m)', fontsize=16)
        plt.ylabel('Received Power (dB, Unknown Ref)', fontsize=16)
        plt.show()

    return n_p, RSS0, e_std

def isfloat(value):
  try:
    float(value)
    return True
  except ValueError:
    return False

# load the data with NumPy function loadtxt
filename = "SurveyTest.csv"
#filename_rss = "survey_20190726_rss.csv"
#filename_distance = "survey_20190726_distance.csv"

coords = {'meb': (40.768796, -111.845994),
'browning': (40.766326, -111.847727),
'ustar': (40.768810, -111.841739),
'bes': (40.761352, -111.846297),
'fm': (40.758060, -111.853314),
'honors': (40.763975, -111.836882),
'smt': (40.767959, -111.831685),
'dentistry': (40.758183, -111.831168),
'law73': (40.761714, -111.851914),
'bookstore': (40.764059, -111.847511),
'humanities': (40.763894, -111.844020),
'web': (40.767902, -111.845535),
'ebc': (40.767611, -111.838103),
'sage': (40.762914, -111.830655),
'madsen': (40.757968, -111.836334),
'moran': (40.769830, -111.837768)}

data = pandas.read_csv(filename, delimiter=",", index_col=0, header=0)
#rss = pandas.read_csv(filename_rss, delimiter=",", index_col=0)
#distance = pandas.read_csv(filename_distance, delimiter=",", index_col=0)

# Load rss and distance values into 1-D numpy arrays 
rss_array = np.array([])
dist_array = np.array([])
for rowlabel, content in data.items(): 
    coord1 = coords[rowlabel]
    for collabel, rss in content.items():
        if (type(rss) is float) or ((type(rss) is str) and isfloat(rss)):
            rss_array = np.append(rss_array, float(rss))
            coord2 = coords[collabel]
            dist_array = np.append(dist_array, calcDistLatLong(coord1, coord2) )


n_p, RSS0, e_std = calcPLexponent(rss_array, dist_array, 1, True)
print('n_p = ', n_p)
print('RSS0 = ', RSS0)
print('std of error = ', e_std)
plt.tight_layout()
plt.savefig('output.png')
input()
