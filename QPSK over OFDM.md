# Transmit and receive a file with QPSK over OFDM

## SDR nodes used for this session

For this session we will make use of the following three pairs of CBRS rooftop SDRs:

| Node 1 | Node 2|
| --- | --- |
| cbrssdr1-browning | cbrssdr1-bes |
| cbrssdr1-meb | cbrssdr1-ustar | 
| cbrssdr1-honors | cbrssdr1-smt |

** Given the limited number of resources we will have to time share access.***

<!---
## Choose/Create a profile with rooftop nodes(x310) on two sites (If not created earlier, else SKIP)

1. Login to POWDER platform.
2. From `Experiments` (top left corner) select `Start Experiment`.
3. Change the default profile to `gnuradio_ota` and click Next.
4. For Parameterize tab, select/edit:
    *  Type of the node paired with the X310 Radios : `d740`.
    * X310 comma separated list : `cbrssdr1-browning, cbrssdr1-meb`.
    * B210/NUC Aggregate|component id : (Leave blank when not using any FE)
5. For Finalize tab, provide a name to your experiment and select a cluster `emulab`.
6. Schedule the period for the experiment (number of hours you want to perform the experiment).
--->

## SSH to the nodes

1. Once the profile is created, go to `List View` option to get the node address.
2. With a remote SSH client login to the nodes. (Must have SSH public key uploaded).
3. In this experiment your group will be assigned one of the node pairs listed above. You will use one of the nodes as the transmitter and the other as the receiver.
4. While SSH make sure the X11 forwarding option is enable.

## Open and edit flowgraph in GNU Radio Companion - transmitter node


GNU Radio provides the building blocks for OFDM modulated signal. We will use these blocks for this experiment.

1. Open transmitter file 
```python
bash -l
gnuradio-companion /proj/mww2019/gnuradio-ofdm-example/TX1.grc
```
3. Save the file in your user directory /users/username/
4. In the `File Source` block, double click to open the properties. In the File option browse `/proj/mww2019/gnuradio-ofdm-example/file_to_transmit.txt`.
5. The transmitter is now ready to transmit with default configuration.

### Brief description of the transmitter blocks

1. **File Source** : Contains the file we want to transmit (image/text/..)
Now we have streams of bytes as output without any boundary that defines the start/end of the packet.

2. **Stream to tagged stream** : Is the block that defines the packet boundary. It converts a regular stream to tagged stream. All this block does is add a length tag 'packet_len' with a value 30 (or as you provide). Which means after every 30 items there will be a tag called 'packet_len'.

3. **Stream CRC32** : The first byte of the packet has a tag 'packet_len' with value 30 which means the number of bytes in the packet. The output is the same as input with a trailing CRC32 (4 bytes) of the packet. The tag is also now reset to the new length.
This is now the payload that we want to transmit. At this step the flow splits up. The top path is for header generator and the bottom is the payload itself.

4. **Protocol Formatter** : Takes in the tagged stream packet and creates a header. This is configurable. In flowgraph, the format object points to an object 'hdr_format'  that we pass to the block. So the block has the conceptual idea on what it does but how it is done is implemented somewhere else. So its something that we do in python code. We generate a OFDM packet header, giving some information on what I want to do. So the right side, it calculates & generates the header and outputs it on this channel. The other channel is used to transport the payload.

5. **Repack bits** : Prepares for modulation (8 bits to 1 bit) for BPSK and (8 bits to 2 bits) for QPSK.
6. **Virtual Sink** : When paired with a Virtual Source block, this is essentially the same as drawing a wire between two blocks. This block can be useful for tidying up a complex flowgraph.
7. **Chunks to symbols** : Maps bits to complex symbols. Both the paths can have different modulation scheme. Here we have done BPSK for header and QPSK for payload. After this we have complex symbols.
8. **Tagged Stream MUX** : Multiplexer : Which still understand packet boundaries. Packets are output sequentially from each input stream. As the input might have different length. The output signal has a new length tag which is the sum of all individual length tags.

9. **OFDM Carrier allocator** : Allocates the sub-carriers/pilot carriers that will be occupied by data or pilot symbols. Add sync words.
10. **IFFT** : Converts frequency into time domain.
11. **OFDM cyclic Prefixer** : Adds guard interval. 
12. **Multiply constant** : reduces the amplitude so that it is within [-1 : +1] range to avoid clipping.
13. **USRP Sink** : Connects to the antenna
    
## Open and edit flowgraph in GNU Radio Companion - receiver node
1. Open receiver file
```python
bash -l
gnuradio-companion /proj/mww2019/gnuradio-ofdm-example/RX_1.grc
```
2. Save the file in /users/username/
3. In the `File Sink` block, double click to open the properties, and replace `sayazm` with your own username in file location.
4. The receiver is now ready to receive and decode the signal.

### Brief description of the receciver blocks

1. **USRP Source** : Connects to the receiver antenna. Outputs complex samples to process.

2. **Schmidl & Cox OFDM synchronization** : Used for timing synchronization to detect the start of the packet and frequency offset correction with the preambles (sync words).

3. **Header/Payload Demux** : Demultilexes the header and payload. It takes in the received signal in the incoming port and drops until a trigger (high signal- non zero, Schmidl & Cox OFDM synchronization finds out the position for packet start) value is received. Once it is triggered the sample are passed to the outgoing port 0. As the length of the header is always knwon/fixed it pipes in the fixed number of header into the first sub-flow to demodulate the header. Once the header is demodulated it feeds back the payload information (eg. length of the payload) so that the payload can be demodulated in the second sub-flow.

4. **FFT** : Convert time to frequency domain.

5. **OFDM Channel Estimation** : Calculates channel state with sync words. 

6. **OFDM Frame Equalizer** : Reverses the effect of channel on the received symbol.

7. **OFDM Serializer** : Inverse block of carrier allocator. It discard the pilot symbols and output the data symbols.

8. **Constellation Decoder** : Decodes constillation points into bits.

9. **Packet Header Parser** : Inverse of packet header generator in transmitter. It posts header metadata as a message to the Header Payload demux block.

## Execute the flowgraph

1. Execute the `receiver` file first by clicking Execute/F6 button on the toolbar. 
2. Execute the `transmit` file.
2. The signal can be viewed in Time/Frequency domain if QT GUI blocks are added after USRP Source at receiver.
3. Hit Kill button or F7 to stop the transmitter/receiver flow after few seconds.
4. Open a new terminal for the receive node and open the the received file.
```python
vim /users/username/rx.txt
```
5. Basic Parameters that you can modify to check performance: Gain (Tx/Rx), Sampling rate, Bandwidth

## Calculate Packet Error Rate

The `Packet header parser` outputs the metadata of the header. We can use a `Message Debug` block to save the output to a file.

1. Use the search option from the toolbar to find `Message debug block` from the right tree pannel.
2. Drag and drop the block in the working panel.
3. Connect the ouput port of Packet Header parser block to the print port of Message Debug block.
4. Click on `Generate the flow graph` button on the toolbar to save the flow as python script. 

5. Run python script generated from the .grc file in receiver. Directing the output to be saved in a text file. Replace the username with your own username
```python
/users/username/OFDM_TX_RX_1.py >> header.txt
```
6. Execute the transmit file

7. Make a copy of the python script PER.py to your user directory
```python
cp /proj/mww2019/gnuradio-ofdm-example/PER.py /users/..
```

8. Run python script to calculate PER
```python
python3 PER.py
```