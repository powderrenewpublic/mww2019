# Over-the-air operation on POWDER

#### Per-group information

We will be using the experiments created in this session for all hands-on sessions during the rest of the day. The table below contains the relevant information and the naming convention you should use to facilitate that.

| Group Number   | Function  | Parameters | Profile Name/Experiment Name |
| --- | --- | --- | --- |
| 1 | x310_node_pair | 0,"cbrssdr1-dentistry","d430" | cbrs-dentistry |
| 2 | x310_node_pair | 0,"cellsdr1-dentistry","d430" | cell-dentistry |
| 3 | x310_node_pair | 0,"cbrssdr1-bes","d430" | cbrs-bes |
| 4 | x310_node_pair | 0,"cellsdr1-bes","d430" | cell-bes |
| 5 | x310_node_pair | 0,"cbrssdr1-browning","d430" | cbrs-browning |
| 6 | x310_node_pair | 0,"cellsdr1-browning","d430" | cell-browning |
| 7 | x310_node_pair | 0,"cbrssdr1-fm","d430" | cbrs-fm |
| 8 | x310_node_pair | 0,"cellsdr1-fm","d430" | cell-fm |
| 9 | x310_node_pair | 0,"cbrssdr1-honors","d430" | cbrs-honors |
| 10 | x310_node_pair | 0,"cellsdr1-honors","d430" | cell-honors |
| 11 | x310_node_pair | 0,"cbrssdr1-meb","d430" | cbrs-meb |
| 12 | x310_node_pair | 0,"cellsdr1-meb","d430" | cell-meb |
| 13 | x310_node_pair | 0,"cbrssdr1-smt","d430" | cbrs-smt |
| 14 | x310_node_pair | 0,"cellsdr1-smt","d430" | cell-smt |
| 15 | x310_node_pair | 0,"cbrssdr1-ustar","d430" | cbrs-ustar |
| 16 | x310_node_pair | 0,"cellsdr1-ustar","d430" | cell-ustar |
| 17 | b210_nuc_pair  | 0,"madsen","nuc2" | madsen |
| 18 | b210_nuc_pair  | 0,"bookstore","nuc2" | bookstore |
| 19 | b210_nuc_pair  | 0,"ebc","nuc2" | ebc |
| 20 | b210_nuc_pair  | 0,"humanities","nuc2" | humanities |
| 21 | b210_nuc_pair  | 0,"law73","nuc2" | law73 |
| 22 | b210_nuc_pair  | 0,"moran","nuc2" | moran |

#### Create a profile for over-the-air operation with GNU Radio

1.  Log into the POWDER portal. Find the `mww_ota` profile. (Click on `Experiments`, select `Start Experiment`, click on `Change Profile` and enter `mww_ota` in the search box.) Or, click [here](https://www.powderwireless.net/p/mww2019/mww_ota).


2. Rather than instantiating the profile, make your own copy by selecting the `Copy Profile` option. **Use the `Profile Name` entry for your group in the table above for the new profile.**

3. Edit the new profile and add the appropriate function call and parameters listed in the table above.

	For example, group 3 will add:
	
	```
	x310_node_pair(0,"cbrssdr1-bes","d430")
	```

	And group 19 will add:
	
	```
	b210_nuc_pair(0,"ebc","nuc2")		
	```

4.  Save the profile and click `Instantiate` to instantiate the profile. **On the `Finalize` page be sure to use the `Experiment Name` for your group listed in the table above.**

5.  Once your experiment is instantiated, verify that your compute node can interact with the SDR/USRP:
	```
	bash -l
	uhd_find_devices
	uhd_usrp_probe
	```

  
