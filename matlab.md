Matlab instructions
===================

Copy/paste the ssh link and add -Y to the end. (e.x. `ssh -p 22 alexo@pc771.emulab.net -Y`)

    cd ~
    chmod 644 .Xauthority
    git clone /share/mww2019
    sudo chown -R matlab mww2019
    sudo -E -u matlab /usr/local/MATLAB/R2019a/bin/matlab

Your tutorial material is available under:

    ~/mww2019/MATLAB_partials_POWDER
